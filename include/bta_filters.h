///  @file bta_filters.h
///  @version 2.2.6
///
///  @brief This header file contains the configuration structs for filters
///
///  Copyright Bluetechnix 2016
///
///  @author Alex Falkensteiner
///
///  @cond svn
///
///  Information of last commit
///  $Rev::               $:  Revision of last commit
///  $Author::            $:  Author of last commit
///  $Date::              $:  Date of last commit
///
///  @endcond
///

#ifndef BTA_FILTERS_H_INCLUDED
#define BTA_FILTERS_H_INCLUDED



typedef void* BTA_FltConfig;
typedef void* BTA_FltHandle;



/// @brief  This enum defines which filter is to be instantiated
typedef enum BTA_FltType {
    BTA_FltTypeIntrinsicUndistort,      ///< Intrinsic lens parameters are used to undistort the data
    BTA_FltTypeMath,                    ///< Basic mathematical operations
    BTA_FltTypeAvgSequences,            ///< Averaging of multiple sequences into one frame
#   ifndef BTA_EXCLUDE_FILTERS
    BTA_FltTypePixelIntrpl,             ///< A pixel can be replaced by the average value of a set of pixels
    BTA_FltTypeMotionDetector,          ///< A sliding average is used to calculate the satic background. Motion is then filtered
    BTA_FltTypeMorphology,              ///< Basic morphology functions (dilation, erosion)
    BTA_FltTypeOrientation,             ///< Filter for rotating and flipping data
    BTA_FltTypeAvgPixels,				///< Average all pixels of input frame seperately for each channel
    BTA_FltTypeDct,                     ///< DCT computation for sharpness factor
    BTA_FltTypeCropChessboard,          ///< If a chessboard is recognized, all is cropped but the chessboard
    BTA_FltTypeLaplace,                 ///< Apply a laplace operator to a channel
    BTA_FltTypeFocusPreproc,            ///< Propretary preprocessing for focus calibration
    BTA_FltTypeCrop,                    ///< Crop to ROI
    BTA_FltTypeCombine,                 ///< Combine channels patching them together
    BTA_FltTypeGaussianBlur,            ///< Apply a gaussian filter
    BTA_FltTypeFindChessboard,          ///< The defined chessboard is found, the corners are drawn on the image data if so specified and the corner data is added as metadata
	BTA_FltTypeRmsFrames,				///< Apply a root mean sqare filter
#   endif
} BTA_FltType;



typedef struct BTA_FltIntrinsicUndistortConfig {
    //BTA_ChannelId channelToProcess;       //TBA
    //BTA_DataFormat dataFormatToProcess;   //TBA
    uint16_t xRes;          ///< this filter is meant for input data of this width (0: all sizes)
    uint16_t yRes;          ///< this filter is meant for input data of this height (0: all sizes)
    float cameraMatrix[9];  ///< 3x3 Matrix: camera matrix
    float distCoeffs[5];    ///< 5x1 Vector: distortion coefficients
} BTA_FltIntrinsicUndistortConfig;



typedef enum BTA_FltMathType {
    BTA_FltMathTypeMultFM1,
    BTA_FltMathTypeAbs,
    BTA_FltMathTypeMax,
} BTA_FltMathType;

typedef struct BTA_FltMathConfig {
    BTA_FltMathType mathType;
    BTA_ChannelId channelToProcess;
    BTA_ChannelId channelIdResult;
    float *dataFM1;                      ///< Float Matrix #1
    uint16_t xResFM1;                    ///< Number of columns of FM1
    uint16_t yResFM1;                    ///< Number of rows of FM1
} BTA_FltMathConfig;



typedef struct BTA_FltAvgSequencesConfig {
    //BTA_ChannelId channel(s)ToProcess ///< The filter currently averages only Distance, X, Y, Z and ORs Flags
    uint16_t averageWindowLength;       ///< The filter expects this many sequences to be averaged
    //values not to average             ///< The filter currently ignores 0 pixels. if the pixel is invalid in half the sequences the whole average is set to 0
} BTA_FltAvgSequencesConfig;


#ifndef BTA_EXCLUDE_FILTERS


typedef struct BTA_FltAvgPixelsConfig {
	BTA_ChannelId channelToProcess;
	BTA_ChannelId channelIdResult;
} BTA_FltAvgPixelsConfig;


typedef struct BTA_FltPixelIntrplConfig {
    uint32_t **pxIndicesIn;
    uint16_t *pxIndicesInLens;
    uint32_t *pxIndicesOut;
    uint16_t pxCount;
} BTA_FltPixelIntrplConfig;



typedef struct BTA_FltMotionDetectorConfig {
    uint16_t slafWindowLength;          ///< The background adaption is done by a sliding average
    uint8_t slafStride;                 ///< The sliding average can be configured to average every nth frame
    uint16_t threshold;                 ///< If the current pixel exceeds the background by this amount: activity
} BTA_FltMotionDetectorConfig;



typedef enum BTA_FltMorphologyType {
    BTA_FltMorphologyTypeDilation,
    BTA_FltMorphologyTypeErosion,
} BTA_FltMorphologyType;

typedef struct BTA_FltMorphologyConfig {
    BTA_ChannelId channelToProcess;
    BTA_FltMorphologyType morphologyType;
    uint8_t *mask;
    uint8_t xRes;
    uint8_t yRes;
} BTA_FltMorphologyConfig;



typedef enum BTA_FltOrientationType {
    BTA_FltOrientationTypeFlipHor,
    BTA_FltOrientationTypeFlipVer,
    BTA_FltOrientationTypeRotate,
} BTA_FltOrientationType;

typedef struct BTA_FltOrientationConfig {
    BTA_FltOrientationType orientationType;
    uint16_t degrees;
} BTA_FltOrientationConfig;



typedef struct BTA_FltBilateralConfig {
    BTA_ChannelId channelToProcess;
} BTA_FltBilateralConfig;



typedef struct BTA_FltDctConfig {
    BTA_ChannelId channelToProcess;
    BTA_ChannelId channelIdResult;
} BTA_FltDctConfig;



typedef struct BTA_FltCropChessboardConfig {
    BTA_ChannelId channelToProcess;
    float scaleFactor;                  ///< upscales or downscales the input before calling findChessboardCorners (should be scaled for functionality and performance)
    uint8_t edgeCountHor;               ///< The number of edges to be fount horizontally
    uint8_t edgeCountVert;              ///< The number of edges to be fount vertically
    float border;                       ///< 0 for no border, <0 for cropping >0 for border
    BTA_ChannelId channelIdResult;
} BTA_FltCropChessboardConfig;



typedef struct BTA_FltLaplaceConfig {
    BTA_ChannelId channelToProcess;
    BTA_ChannelId channelIdResult;
} BTA_FltLaplaceConfig;



typedef struct BTA_FltFocusPreprocConfig {
    BTA_ChannelId channelToProcess;
    float scaleFactorForChessboard;
    uint8_t edgeCountHorForChessboard;
    uint8_t edgeCountVertForChessboard;
    BTA_ChannelId channelIdResult;
} BTA_FltFocusPreprocConfig;


typedef struct BTA_FltCropConfig {
	BTA_ChannelId channelToProcess;
    float roiStartX;                ///< x coordinate for ROI to preserve including this coordinate. If relative (0 >= x < 1) -> multiplied by xRes.
    float roiStartY;                ///< x coordinate for ROI to preserve including this coordinate. If relative (0 >= x < 1) -> multiplied by xRes.
    float roiEndX;                  ///< x coordinate for ROI to preserve including this coordinate. If relative (0 >= x < 1) -> multiplied by xRes.
    float roiEndY;                  ///< x coordinate for ROI to preserve including this coordinate. If relative (0 >= x < 1) -> multiplied by xRes.
	BTA_ChannelId channelIdResult;
} BTA_FltCropConfig;


typedef struct BTA_FltCombineConfig {
    BTA_ChannelId channelToProcess;
    uint8_t removeOriginals;
} BTA_FltCombineConfig;


typedef struct BTA_FltGaussianBlurConfig {
    BTA_ChannelId channelToProcess;
    BTA_ChannelId channelIdResult;
} BTA_FltGaussianBlurConfig;


typedef struct BTAFltFindChessboardConfig {
    BTA_ChannelId channelToProcess;
    float scaleFactor;                  ///< upscales or downscales the input before calling findChessboardCorners (should be scaled for functionality and performance)
    uint8_t edgeCountHor;               ///< The number of edges to be fount horizontally
    uint8_t edgeCountVert;              ///< The number of edges to be fount vertically
} BTA_FltFindChessboardConfig;

typedef struct BTAFltRmsFramesConfig {
	BTA_ChannelId channelToProcess;
	BTA_ChannelId channelIdResult;
	uint16_t windowLength;				///< The number frames averaged
} BTA_FltRmsFramesConfig;
#endif
#endif
