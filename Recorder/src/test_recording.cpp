//
// Created by Joël Dumoulin on 11.06.17.
//


#include "KinectRecorder.h"
#include "Argos3DRecorder.h"
#include "RecordingConf.h"

#include <iostream>

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/algorithm/string.hpp>

#include <string>

using namespace std;
using boost::asio::ip::tcp;

int main(int argc, char *argv[]) {
    RecordingConf recordingConf("/Users/joel/dev/projects/unicity/out", "Kinect01", "01", "001");

    KinectRecorder kinectRecorder{};

    kinectRecorder.initSensor();
    kinectRecorder.saveParameters(recordingConf.getKinectParametersPath().string());

    //kinectRecorder.showFrames();

    std::this_thread::sleep_for(std::chrono::milliseconds(2000));



    kinectRecorder.startRecording(recordingConf);

    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    kinectRecorder.stopRecording();

    kinectRecorder.stop();

    return 0;

}