//
// Created by Joël Dumoulin on 11.06.17.
//

#include "KinectRecorder.h"

#include <iostream>
#include <thread>
#include <chrono>

#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/console/time.h>
#include <pcl/io/ply_io.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include "yaml-cpp/yaml.h"

KinectRecorder::KinectRecorder() : AbstractRecorder() {}

KinectRecorder::~KinectRecorder() {}

using namespace std;
using namespace chrono;

bool KinectRecorder::initSensor(){
    std::cout << "Initializing Kinect sensor" << std::endl;

    Processor freenectprocessor = OPENGL;
    grabber = new K2G(freenectprocessor);

    boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloud;
    cloud = grabber->getCloud();
    cloud->sensor_orientation_.w() = 0.0;
    cloud->sensor_orientation_.x() = 1.0;
    cloud->sensor_orientation_.y() = 0.0;
    cloud->sensor_orientation_.z() = 0.0;

    status = SensorStatus::ready;

    std::cout << "Sensor initialized!" << std::endl;

    recordingWorker = std::thread(&KinectRecorder::run, this);

    return true;
}

void KinectRecorder::run() {
    grabber->printParameters();
    std::cout << "Kinect sensor grabbing frames!!!" << std::endl;

    std::chrono::high_resolution_clock::time_point p;
    std::string now;
    char duration[17];
    char strFrameNb[17];
    milliseconds nowTime, durationTime;

    cv::Mat color, depth, ir;
    cv::Mat color_hd;

    boost::filesystem::path path;

    while (status == SensorStatus::ready || status == SensorStatus::recording) {
        if (status == SensorStatus::recording) {
            // Get new frame
            if(ENABLE_RGB_HD){
                grabber->get(color, color_hd, depth, ir, true);
            }else{
                grabber->get(color, depth, ir, false);
            }

            frameNb++;

            // Compute timestamp
            nowTime = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
            now = std::to_string(nowTime.count());

            durationTime = nowTime - startTime;

            sprintf(duration, "%05lu", size_t(durationTime.count()));

            sprintf(strFrameNb, "%05ld", frameNb);

            // Save frames for each activated channel
            if (ENABLE_RGB) {
                path = recordingConf->getColorPath();
                path /= now + "_" + string(duration) + "_" + string(strFrameNb);
                frameWriter.add(color, path.string(), FrameType::color, cv::ROTATE_180);
            }

            if (ENABLE_RGB_HD) {
                path = recordingConf->getColorHdPath();
                path /= now + "_" + string(duration) + "_" + string(strFrameNb);
                frameWriter.add(color_hd, path.string(), FrameType::color_hd, cv::ROTATE_180);
            }

            if (ENABLE_DEPTH) {
                path = recordingConf->getDepthPath();
                path /= now + "_" + string(duration) + "_" + string(strFrameNb);
                frameWriter.add(depth, path.string(), FrameType::depth, cv::ROTATE_180);
            }

            if (ENABLE_IR) {
                path = recordingConf->getIrPath();
                path /= now + "_" + string(duration) + "_" + string(strFrameNb);
                frameWriter.add(ir, path.string(), FrameType::ir, cv::ROTATE_180);
            }
        }
    }

    frameWriter.stop();
    grabber->shutDown();

    std::cout << "Stopped" << std::endl;
}

void KinectRecorder::saveParameters(std::string kinectParametersPath){
    libfreenect2::Freenect2Device::IrCameraParams irParams = grabber->getIrParameters();
    libfreenect2::Freenect2Device::ColorCameraParams colorParams = grabber->getRgbParameters();

    YAML::Emitter out;
    out << YAML::BeginMap;

        out << YAML::Key << "ir";

            out << YAML::Value << YAML::BeginMap;

            out << YAML::Key << "fx";
            out << YAML::Value << std::to_string(irParams.fx);

            out << YAML::Key << "fy";
            out << YAML::Value << std::to_string(irParams.fy);

            out << YAML::Key << "cx";
            out << YAML::Value << std::to_string(irParams.cx);

            out << YAML::Key << "cy";
            out << YAML::Value << std::to_string(irParams.cy);

            out << YAML::Key << "k1";
            out << YAML::Value << std::to_string(irParams.k1);

            out << YAML::Key << "k2";
            out << YAML::Value << std::to_string(irParams.k2);

            out << YAML::Key << "k3";
            out << YAML::Value << std::to_string(irParams.k3);

            out << YAML::Key << "p1";
            out << YAML::Value << std::to_string(irParams.p1);

            out << YAML::Key << "p2";
            out << YAML::Value << std::to_string(irParams.p2);

            out << YAML::EndMap;

        out << YAML::Key << "color";

            out << YAML::Value << YAML::BeginMap;

            out << YAML::Key << "fx";
            out << YAML::Value << std::to_string(colorParams.fx);

            out << YAML::Key << "fy";
            out << YAML::Value << std::to_string(colorParams.fy);

            out << YAML::Key << "cx";
            out << YAML::Value << std::to_string(colorParams.cx);

            out << YAML::Key << "cy";
            out << YAML::Value << std::to_string(colorParams.cy);

            out << YAML::EndMap;

    out << YAML::EndMap;

    std::ofstream fout(kinectParametersPath);
    fout << out.c_str();
}