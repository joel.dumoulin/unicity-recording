//
// Created by Joël Dumoulin on 11.06.17.
//


#include "KinectRecorder.h"
#include "Argos3DRecorder.h"
#include "FotonicRecorder.h"
#include "RecordingConf.h"

#include <iostream>

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/algorithm/string.hpp>

#include <string>
#include <thread>

using namespace std;
using boost::asio::ip::tcp;

int main(int argc, char *argv[]) {
    std::string recordingPath = "/temp/out";

    std::string usage = "Usage: ./Recorder sensorType port destinationPath\nSensorType: k=Kinect ; a=Argos3D; f=Fotonic";

    std::string sensorType = "k";
    std::string port = "1234";

    if( argc == 4 ) {
        sensorType = argv[1];
        port = argv[2];
        recordingPath = argv[3];
    } else {
        std::cout << "Two argument expected. " << usage << std::endl;
        std::cout << "Using default values: sensorType=Kinect ; port=1234 ; destinationPath=/temp/out";
    }

    AbstractRecorder* recorder;

    if(sensorType.compare("k") == 0){
        recorder = new KinectRecorder{};
    }else if(sensorType.compare("a") == 0){
#ifdef LINUX
        recorder = new Argos3DRecorder{};
#endif
    }else{
#ifdef LINUX
        recorder = new FotonicRecorder{};
#endif
    }


    // Init sensor
    recorder->initSensor();
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    while(true){
        try
        {
            boost::asio::io_service io_service;

            tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), std::stoi(port)));

            tcp::socket socket(io_service);
            acceptor.accept(socket);

            boost::array<char, 128> buf;
            boost::system::error_code error;

            size_t len;

            std::string message;

            RecordingConf* recordingConf;


            while(true){
                std::cout << "Waiting for manager instruction..." << std::endl;

                len = socket.read_some(boost::asio::buffer(buf), error);

                if(error.value() == 0){
                    message = std::string(buf.data(), len);

                    vector<string> strs;
                    boost::split(strs, message, boost::is_any_of(";"));

                    int nbFrames = -1;
                    std::string msg;

                    if(strs[0].compare("startRecording") == 0){
                        std::string sensorId = strs[1];
                        std::string scenarioId = strs[2];
                        std::string idxstr = strs[3];
                        std::cout << "Manager instruction: startRecording (sensorId: " << sensorId << "; scenarioId: " << scenarioId << ";idxstr: "<<idxstr << ")" << std::endl;
                        recordingConf = new RecordingConf(recordingPath, sensorId, scenarioId, idxstr);
                        recorder->startRecording(*recordingConf);
                        msg = "1";
                    }else if(strs[0].compare("stopRecording") == 0){
                        std::cout << "Manager instruction: stopRecording" << std::endl;
                        nbFrames = recorder->stopRecording();
                        msg = std::to_string(nbFrames);
                    }else if(strs[0].compare("stop") == 0){
                        std::cout << "Manager instruction: stop" << std::endl;
                        recorder->stop();
                        return 0;
                    }

                    try {
                        boost::system::error_code ignored_error;
                        boost::asio::write(socket, boost::asio::buffer(msg), ignored_error);
                    }catch (std::exception& e){
                        std::cout << "Exception while trying to answer..." << std::endl;
                        std::cerr << e.what() << std::endl;
                    }
                }else if(error.value() == 2){
                    std::cout << "Socket closed. Reconnecting..." << std::endl;
                    acceptor.close();
                    break;
                }else{
                    std::cout << "Error while reading socket. Quitting..." << std::endl;
                    return 1;
                }
            }
        } catch (std::exception& e)
        {
            std::cerr << e.what() << std::endl;
        }
    }

    return 0;
}