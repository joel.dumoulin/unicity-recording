# ----------------------------------------------------------------------------------------------------------------------
# Includes
# ----------------------------------------------------------------------------------------------------------------------
include_directories(${UnicityRecording_SOURCE_DIR}/include)
include_directories(${UnicityRecording_SOURCE_DIR}/common/include)
include_directories(${UnicityRecording_SOURCE_DIR}/Recorder/include)

# ----------------------------------------------------------------------------------------------------------------------
# OpenCV
# ----------------------------------------------------------------------------------------------------------------------
if(MACOS)
    set(CMAKE_PREFIX_PATH "/usr/local/opt/opencv@3/share/OpenCV")
    set(OpenCV_INCLUDE_DIRS "/usr/local/opt/opencv@3/include")
    set(OpenCV_LIBS "/usr/local/opt/opencv@3/lib")
endif()

# Find OpenCV, you may need to set OpenCV_DIR variable
# to the absolute path to the directory containing OpenCVConfig.cmake file
# via the command line or GUI
find_package(OpenCV REQUIRED)

# If the package has been found, several variables will
# be set, you can find the full list with descriptions
# in the OpenCVConfig.cmake file.
# Print some message showing some of them
message(STATUS "OpenCV library status:")
message(STATUS "    version: ${OpenCV_VERSION}")
message(STATUS "    libraries: ${OpenCV_LIBS}")
message(STATUS "    include path: ${OpenCV_INCLUDE_DIRS}")

# Add OpenCV headers location to your include paths
include_directories(${OpenCV_INCLUDE_DIRS})

# ----------------------------------------------------------------------------------------------------------------------
# Boost ASIO
# ----------------------------------------------------------------------------------------------------------------------
set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREAD ON)
find_package(Boost 1.64.0 REQUIRED system filesystem)
if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS})
    link_directories(${Boost_LIBRARY_DIRS})
endif(Boost_FOUND)

set(Boost_LIBS ${Boost_SYSTEM_LIBRARY} ${Boost_FILESYSTEM_LIBRARY})

# ----------------------------------------------------------------------------------------------------------------------
# Kinect with PCL grabber (https://github.com/giacomodabisias/libfreenect2pclgrabber)
# ----------------------------------------------------------------------------------------------------------------------
option(WITH_PCL "adds pcl cloud support" ON)
if(${WITH_PCL})
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DWITH_PCL")
    find_package(PCL REQUIRED)
    link_directories(${PCL_LIBRARY_DIRS})
    add_definitions(${PCL_DEFINITIONS})
    include_directories(${PCL_INCLUDE_DIRS})
endif()

find_package(Freenect2  REQUIRED)

include_directories(${FREENECT2_INCLUDE_DIRS})

set(Kinect_LIBS ${FREENECT2_LIBRARY} ${PCL_LIBRARIES})

# ----------------------------------------------------------------------------------------------------------------------
# ARGOS
# ----------------------------------------------------------------------------------------------------------------------
if(LINUX)
    add_definitions(-DPLAT_LINUX)
    set(Argos_LIBS ${LIB_PATH}/Lin_x64/libbta.so)
elseif(MACOS)
    add_definitions(-DPLAT_LINUX)
    set(Argos_LIBS ${LIB_PATH}/Lin_x64/libbta.so)
endif()

# ----------------------------------------------------------------------------------------------------------------------
# FOTONIC
# ----------------------------------------------------------------------------------------------------------------------
if(LINUX)
    set(ARCH "x64" CACHE STRING "Platform architecture")

    set(ENABLE_GENICAM ON)
    set(DEPDIR "${UnicityRecording_SOURCE_DIR}/fotonic")
    set(COMMON_BIN_INSTALL ".")

    #
    # genicam
    #



    set(FOTONIC_FRAMEWORK_LIB "${DEPDIR}/fotonic.gentl.cti" CACHE STRING "Path to Fotonic Framework library")
    if(${ARCH} STREQUAL "arm")
        set(GenICam_DIR "${DEPDIR}/libgenicam")
        set(GenICam_LIBRARY_REL_DIR "bin/Linux32_armhf")
        set(GenICam_LIBRARY GenApi_gcc46_v3_0 GCBase_gcc46_v3_0)
    elseif(${ARCH} STREQUAL "x86")
        set(GenICam_DIR "${DEPDIR}/libgenicam")
        set(GenICam_LIBRARY_REL_DIR "bin/Linux32_i86")
        set(GenICam_LIBRARY GenApi_gcc421_v3_0 GCBase_gcc421_v3_0)
    elseif(${ARCH} STREQUAL "x64")
        set(GenICam_DIR "${DEPDIR}/libgenicam")
        set(GenICam_LIBRARY_REL_DIR "bin/Linux64_x64")
        set(GenICam_LIBRARY GenApi_gcc421_v3_0 GCBase_gcc421_v3_0)
    endif()
    set(GenICam_BINARY_REL_DIR "${GenICam_LIBRARY_REL_DIR}")


    set(GenICam_INCLUDE_DIRS "${GenICam_DIR}/library/CPP/include")
    set(GenTL_INCLUDE_DIRS "${DEPDIR}/libgentl")

    set(GenICam_LIBRARY_DIR "${GenICam_DIR}/${GenICam_LIBRARY_REL_DIR}")
    set(GenICam_BINARY_DIR "${GenICam_DIR}/${GenICam_BINARY_REL_DIR}")


    add_definitions("-DGENICAM_USER_ALWAYS_LINK_RELEASE")

    if(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")

        add_definitions("-std=c++11 -Werror -Wno-deprecated-declarations")
        if(CMAKE_BUILD_TYPE STREQUAL "Debug")
            set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread  --coverage")
            set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} --coverage")
        endif()

    endif()

    include_directories(${GenTL_INCLUDE_DIRS})
    include_directories(${GenICam_INCLUDE_DIRS})

    link_directories(${GenICam_LIBRARY_DIR})
    link_directories(${DEPDIR})

    message (STATUS "GENICAM IS ENABLED")
elseif(MACOS)
    set(DEPDIR "${UnicityRecording_SOURCE_DIR}/fotonic")
    set(GenTL_INCLUDE_DIRS "${DEPDIR}/libgentl")

    set(GenICam_DIR "${DEPDIR}/libgenicam")
    set(GenICam_INCLUDE_DIRS "${GenICam_DIR}/library/CPP/include")

    include_directories(${GenTL_INCLUDE_DIRS})
    include_directories(${GenICam_INCLUDE_DIRS})
endif()


# ----------------------------------------------------------------------------------------------------------------------
# YAML-CPP
# ----------------------------------------------------------------------------------------------------------------------
if(MACOS)
    include_directories(/usr/local/include/yaml-cpp)
elseif(LINUX)
    include_directories(/usr/include/yaml-cpp)
endif()


# ----------------------------------------------------------------------------------------------------------------------
# Executables
# ----------------------------------------------------------------------------------------------------------------------
if(LINUX)
    add_executable(Recorder main.cpp AbstractRecorder.cpp KinectRecorder.cpp k2g.cpp Argos3DRecorder.cpp FotonicRecorder.cpp FrameWriter.cpp DataFrame.cpp RecordingConf.cpp)
    add_executable(TestRecorder test_recording.cpp AbstractRecorder.cpp KinectRecorder.cpp k2g.cpp Argos3DRecorder.cpp FotonicRecorder.cpp FrameWriter.cpp DataFrame.cpp RecordingConf.cpp)
elseif(MACOS)
    add_executable(Recorder main.cpp AbstractRecorder.cpp KinectRecorder.cpp k2g.cpp FrameWriter.cpp DataFrame.cpp RecordingConf.cpp)
    add_executable(TestRecorder test_recording.cpp AbstractRecorder.cpp KinectRecorder.cpp k2g.cpp FrameWriter.cpp DataFrame.cpp RecordingConf.cpp)
endif()

# ----------------------------------------------------------------------------------------------------------------------
# Link libraries
# ----------------------------------------------------------------------------------------------------------------------
target_link_libraries(Recorder ${OpenCV_LIBS})
target_link_libraries(Recorder ${Boost_LIBS})
target_link_libraries(Recorder ${Kinect_LIBS})
target_link_libraries(Recorder yaml-cpp)

if(LINUX)
    target_link_libraries(Recorder ${Argos_LIBS})

    # Fotonic
    if(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
        target_link_libraries(Recorder ${FOTONIC_FRAMEWORK_LIB} ${GenICam_LIBRARY} rt)
    else()
        target_link_libraries(Recorder ${FOTONIC_FRAMEWORK_LIB} ${GenICam_LIBRARY})
    endif()

    set_target_properties(Recorder PROPERTIES INSTALL_RPATH "$ORIGIN/;$ORIGIN/libgenicam/${GenICam_LIBRARY_REL_DIR}")

    file(
            GLOB
            DLL_DEPENDENCIES
            ${DEPDIR}/*${CMAKE_SHARED_LIBRARY_SUFFIX}
    )

    install(TARGETS Recorder DESTINATION ${COMMON_BIN_INSTALL} COMPONENT ${COMPONENT_NAME})

    foreach(dllfile ${DLL_DEPENDENCIES})
        install(FILES ${dllfile} DESTINATION ${COMMON_BIN_INSTALL} COMPONENT ${COMPONENT_NAME})
    endforeach()

endif()


target_link_libraries(TestRecorder ${OpenCV_LIBS})
target_link_libraries(TestRecorder ${Boost_LIBS})
target_link_libraries(TestRecorder ${Kinect_LIBS})
target_link_libraries(TestRecorder yaml-cpp)

if(LINUX)
    target_link_libraries(TestRecorder ${Argos_LIBS})

    # Fotonic
    if(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
        target_link_libraries(TestRecorder ${FOTONIC_FRAMEWORK_LIB} ${GenICam_LIBRARY} rt)
    else()
        target_link_libraries(TestRecorder ${FOTONIC_FRAMEWORK_LIB} ${GenICam_LIBRARY})
    endif()

    set_target_properties(TestRecorder PROPERTIES INSTALL_RPATH "$ORIGIN/;$ORIGIN/libgenicam/${GenICam_LIBRARY_REL_DIR}")

    file(
            GLOB
            DLL_DEPENDENCIES
            ${DEPDIR}/*${CMAKE_SHARED_LIBRARY_SUFFIX}
    )

    install(TARGETS TestRecorder DESTINATION ${COMMON_BIN_INSTALL} COMPONENT ${COMPONENT_NAME})

    foreach(dllfile ${DLL_DEPENDENCIES})
        install(FILES ${dllfile} DESTINATION ${COMMON_BIN_INSTALL} COMPONENT ${COMPONENT_NAME})
    endforeach()
endif()
