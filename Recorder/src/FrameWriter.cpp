//
// Created by Joël Dumoulin on 11.06.17.
//

#include "FrameWriter.h"

using namespace cv;
using namespace std;

FrameWriter::FrameWriter() : run(true), queue() {}

FrameWriter::~FrameWriter() {}

void FrameWriter::worker() {
    while (this->run) {
        while (queue.size() > 0) {
            DataFrame *dataFrame = queue.remove();

            cv::Mat data = dataFrame->getData();

            if(dataFrame->getRotate() == cv::ROTATE_180){
                cv::rotate(data, data, dataFrame->getRotate());
            }

            switch (dataFrame->getFrameType()) {
                case FrameType::color:
                    try {
                        cv::imwrite(dataFrame->getPath() + ".bmp", data);
                    }
                    catch (runtime_error &ex) {
                        std::cout << "Exception converting image to PNG format: " << ex.what() << std::endl;
                    }
                    break;
                case FrameType::color_hd: {
                    try {
                        cv::imwrite(dataFrame->getPath() + ".bmp", data);
                    }
                    catch (runtime_error &ex) {
                        std::cout << "Exception converting image to PNG format: " << ex.what() << std::endl;
                    }
                    break;
                }
                case FrameType::depth:
                case FrameType::ir: {
                    matwrite(dataFrame->getPath() + ".bin", data);
                    break;
                }
            }

            std::cout << "-";

            delete dataFrame;
        }
    }
}

void FrameWriter::add(cv::Mat data, std::string path, FrameType frameType, cv::RotateFlags rotate) {
    DataFrame *dataFrame = new DataFrame(data, path, frameType, rotate);
    queue.add(dataFrame);
    std::cout << "+";
}

void FrameWriter::stop() {
    run = false;
}

int FrameWriter::getSize() {
    return queue.size();
}

void FrameWriter::matwrite(const string& filename, const cv::Mat& mat)
{
    ofstream fs(filename, fstream::binary);

    // Header
    int type = mat.type();
    int channels = mat.channels();
    fs.write((char*)&mat.rows, sizeof(int));    // rows
    fs.write((char*)&mat.cols, sizeof(int));    // cols
    fs.write((char*)&type, sizeof(int));        // type
    fs.write((char*)&channels, sizeof(int));    // channels

    // Data
    if (mat.isContinuous())
    {
        fs.write(mat.ptr<char>(0), (mat.dataend - mat.datastart));
    }
    else
    {
        int rowsz = CV_ELEM_SIZE(type) * mat.cols;
        for (int r = 0; r < mat.rows; ++r)
        {
            fs.write(mat.ptr<char>(r), rowsz);
        }
    }
}

cv::Mat FrameWriter::matread(const string& filename)
{
    ifstream fs(filename, fstream::binary);

    // Header
    int rows, cols, type, channels;
    fs.read((char*)&rows, sizeof(int));         // rows
    fs.read((char*)&cols, sizeof(int));         // cols
    fs.read((char*)&type, sizeof(int));         // type
    fs.read((char*)&channels, sizeof(int));     // channels

    // Data
    cv::Mat mat(rows, cols, type);
    fs.read((char*)mat.data, CV_ELEM_SIZE(type) * rows * cols);

    return mat;
}