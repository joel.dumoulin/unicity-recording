//
// Created by Joël Dumoulin on 11.06.17.
//

#include "Argos3DRecorder.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bta.h>

#include <thread>
#include <chrono>

#ifdef PLAT_WINDOWS
    #include <windows.h>
#elif defined PLAT_LINUX
    #include <unistd.h>
    #include <time.h>
#else
    #error "No platform defined"
#endif


using namespace std;
using namespace chrono;

Argos3DRecorder::Argos3DRecorder() : AbstractRecorder() {}

Argos3DRecorder::~Argos3DRecorder() {}

bool Argos3DRecorder::initSensor() {
    std::cout << "Initializing Argos3D sensor" << std::endl;

    BTA_Status btaStatus;


    // Initialization
    //----------------------------------------------------------------------------------------------
    // First, the library must be configured via the configuration c-structure.
    // The configuration structure must be initialized with standard values using the function
    // BTAinitConfig. The specific implementation of the library defines which parameters are
    // required and which can be left out. The required parameters must then be set to a valid
    // value before calling BTAopen.

    BTA_Config config;
    printf("BTAinitConfig()\n");
    btaStatus = BTAinitConfig(&config);
    errorHandling(btaStatus);

    // Connection Parameters
    //----------------------------------------------------------------------------------------------
    // Depending on the library and the device, different connection parameters must be set.
    // Redundant connections (like TCP and UDP control connections) are connected sequencially and
    // exclusively
    // (UDP is tried first and only if it fails, TCP is connected)
    // Unnecessary information is ignored (BtaP100Lib ignores ethernet parameters)



    // Specify the interface
    //----------------------------------------------------------------------------------------------
    config.deviceType = BTA_DeviceTypeGenericEth;

    // BtaEthLib only
    // UDP data connection (normally multicast address)
    uint8_t udpDataIpAddr[] = { 224, 0, 0, 1 };
    config.udpDataIpAddr = udpDataIpAddr;
    config.udpDataIpAddrLen = 4;
    config.udpDataPort = 10002;
    // TCP control connection (device's IP address)
    uint8_t tcpDeviceIpAddr[] = { 192, 168, 0, 10 };
    config.tcpDeviceIpAddr = tcpDeviceIpAddr;
    config.tcpDeviceIpAddrLen = 4;
    config.tcpControlPort = 10001;
    // UDP control outbound connection (device's IP address)
    uint8_t udpControlOutIpAddr[] = { 192, 168, 0, 10 };
    config.udpControlOutIpAddr = udpControlOutIpAddr;
    config.udpControlOutIpAddrLen = 4;
    config.udpControlPort = 10003;
    // UDP control inbound connection (normally the host's IP address)
    uint8_t udpControlInIpAddr[] = { 192, 168, 0, 2 };
    config.udpControlInIpAddr = udpControlInIpAddr;
    config.udpControlInIpAddrLen = 4;
    config.udpControlCallbackPort = 10004;




    // If you want to receive btaStatus updates from the library,
    // register a callback function for informative events.
    // This is most useful to get detailed error description(s)
//    config.infoEventEx = &infoEventEx;
    // ...and set the verbosity for infoEvents (the higher the more messages).
//    config.verbosity = 4;

    // If you want to receive the frames immediately when they arrive,
    // register a callback function for incoming frames.
    // You can use the frameArrived callback:
//    config.frameArrived = &frameArrived;
    // But we don't want to do that, we want the extendet version where we also get the handle
    // like this we can identify the connection session in case we have several
//    config.frameArrivedEx = &frameArrivedEx;
    // If both are defined, only the extendet callback is called
    // (The same rules apply to infoEvents)

    // Choose whether and how queueing of frames is done.
    // Please read the BltTofApi SUM section Finding the right configuration in order to
    // find out if queueing makes sense for your device and application
    // We choose 'DropOldest', so we always get the most recent frame
    config.frameQueueMode = BTA_QueueModeDropOldest;
    // Set the length of the frame queue.
    config.frameQueueLength = 3;

    // The frame mode can be configured in order to get the desired data channels in a frame from
    // the sensor / library:
    config.frameMode = BTA_FrameModeDistAmp;

    // Select camera by serial number
//    config.serialNumber = 120;  // the SDK accepts the connection only if the serial number matches
    config.serialNumber = 0;    // any

    // Select camera by serial product order number (PON)
//    config.pon = (uint8_t *)"150-3001";  // the SDK accepts the connection only if the PON matches
    config.pon = 0;    // any


    // Connecting
    //----------------------------------------------------------------------------------------------
    // Now that the configuration structure is filled in, the connection is ready to be opened
    // The first infoEvents should fire while the library is connecting to the sensor.
    printf("BTAopen()\n");
    btaHandle = new BTA_Handle{};
    btaStatus = BTAopen(&config, btaHandle);
    errorHandling(btaStatus);

    // Connection Status
    //----------------------------------------------------------------------------------------------
    // It is possible to get the service state and connection state (not all connections support this)

    printf("Service running: %d\n", BTAisRunning(*btaHandle));
    printf("Connection up: %d\n", BTAisConnected(*btaHandle));

    // Frame-rate
    //----------------------------------------------------------------------------------------------
    // Read and change the frame rate via these functions

//    float frameRate;
//    printf("BTAgetFrameRate()\n");
//    btaStatus = BTAgetFrameRate(btaHandle, &frameRate);
//    errorHandling(btaStatus);
//    printf("Framerate is %f\n", frameRate);
//    printf("BTAsetFrameRate(%f)\n", frameRate);
//    btaStatus = BTAsetFrameRa
//
// te(btaHandle, frameRate);
//    errorHandling(btaStatus);


    // Frame mode
    //----------------------------------------------------------------------------------------------
    // The frame mode defines what channels the sonsor delivers to the library and/or
    // what data the library puts into a frame

    printf("BTAsetFrameMode(BTA_FrameModeXYZAmp)\n");
    btaStatus = BTAsetFrameMode(*btaHandle, BTA_FrameModeXYZAmp);
    errorHandling(btaStatus);

    status = SensorStatus::ready;

    std::cout << "Sensor initialized!" << std::endl;

    recordingWorker = std::thread(&Argos3DRecorder::run, this);

    return true;
}

void Argos3DRecorder::run() {

    std::cout << "Argos3D sensor grabbing frames!!!" << std::endl;

    BTA_Frame *frame;
    BTA_Status btaStatus;

    std::chrono::high_resolution_clock::time_point p;
    std::string now;
    char duration[17];
    char strFrameNb[17];
    milliseconds nowTime, durationTime;

    cv::Mat depth, amplitude;

    boost::filesystem::path path;

    while(status == SensorStatus::ready || status == SensorStatus::recording){
        if (status == SensorStatus::recording) {
            btaStatus = BTAgetFrame(*btaHandle, &frame, 300);
            errorHandling(btaStatus);

            // Compute timestamp
            nowTime = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
            now = std::to_string(nowTime.count());

            durationTime = nowTime - startTime;

            sprintf(duration, "%05lu", size_t(durationTime.count()));

            sprintf(strFrameNb, "%05ld", frameNb);

            frameNb++;

            if (ENABLE_DEPTH) {
                this->getDepth(frame, depth);
                path = recordingConf->getDepthPath();
                path /= now + "_" + string(duration) + "_" + string(strFrameNb);
                frameWriter.add(depth, path.string(), FrameType::depth, cv::ROTATE_180);
            }

            if (ENABLE_AMPLITUDE) {
                this->getAmplitudes(frame, amplitude);
                path = recordingConf->getIrPath();
                path /= now + "_" + string(duration) + "_" + string(strFrameNb);
                frameWriter.add(amplitude, path.string(), FrameType::ir, cv::ROTATE_180);
            }
        }
    }

    frameWriter.stop();

    // Disconnecting
    //----------------------------------------------------------------------------------------------
    // When work is done and no other threads need to access the library's functions,
    // disconnect the sensor and stop the service by simply calling BTAclose
    printf("BTAclose()\n");
    btaStatus = BTAclose(btaHandle);
    errorHandling(btaStatus);

    std::cout << "Stopped" << std::endl;
}

void Argos3DRecorder::getDepth(BTA_Frame *frame, cv::Mat &data){
    BTA_Status btaStatus;
    BTA_DataFormat dataFormat;
    BTA_Unit unit;
    uint16_t xRes, yRes;
    void *xCoordinates, *yCoordinates, *zCoordinates;

    btaStatus = BTAgetXYZcoordinates(frame, &xCoordinates, &yCoordinates, &zCoordinates,
                                  &dataFormat, &unit, &xRes, &yRes);
    errorHandling(btaStatus);
    if (dataFormat == BTA_DataFormatSInt16) {
        // This dataformat tells us that it's ok to cast (void *) to (int16_t *)
        data = cv::Mat::zeros(yRes, xRes, CV_16UC1);
        if (unit == BTA_UnitMillimeter) {
            // -> cast the void* to int16_t* and access data points simply as:
            //   ((int16_t *)xCoordinates)[i]
            //   ((int16_t *)yCoordinates)[i]
            //   ((int16_t *)zCoordinates)[i]
            uint32_t radiusMin = 0xffffffff;
            int nearestPixelX = -1, nearestPixelY = -1;
            int16_t xCoordinate = 0, yCoordinate = 0, zCoordinate = 0;
            for (int y = 0; y < yRes; y++) {
                for (int x = 0; x < xRes; x++) {
                    if (((int16_t *)zCoordinates)[x + y*xRes] > 1) {
                        uint32_t radius = 0;
                        radius += ((int16_t *)xCoordinates)[x + y*xRes] * ((int16_t *)xCoordinates)[x + y*xRes];
                        radius += ((int16_t *)yCoordinates)[x + y*xRes] * ((int16_t *)yCoordinates)[x + y*xRes];
                        radius += ((int16_t *)zCoordinates)[x + y*xRes] * ((int16_t *)zCoordinates)[x + y*xRes];

                        radiusMin = radius;
//                            nearestPixelX = x;
//                            nearestPixelY = y;
//                            xCoordinate = ((int16_t *)xCoordinates)[x + y*xRes];
//                            yCoordinate = ((int16_t *)yCoordinates)[x + y*xRes];
                        zCoordinate = ((int16_t *)zCoordinates)[x + y*xRes];

                        data.at<int16_t>(y, x) = zCoordinate;
                    }
                }
            }
        }
    }
    else if (dataFormat == BTA_DataFormatFloat32) {
        // This dataformat tells us that it's ok to cast (void *) to (float *)
        data = cv::Mat::zeros(yRes, xRes, CV_32FC1);
        if (unit == BTA_UnitMeter) {
            // -> cast the void* to (float *) and access data points simply as:
            //   ((float *)xCoordinates)[i]
            //   ((float *)yCoordinates)[i]
            //   ((float *)zCoordinates)[i]
            float radius2Min = (float)0xffffffff;
            int nearestPixelX = -1, nearestPixelY = -1;
            float xCoordinate = 0, yCoordinate = 0, zCoordinate = 0;
            for (int y = 0; y < yRes; y++) {
                for (int x = 0; x < xRes; x++) {
                    if (((float *)zCoordinates)[x + y*xRes] > 0) {
                        float radius2 = 0;
                        radius2 += ((float *)xCoordinates)[x + y*xRes] * ((float *)xCoordinates)[x + y*xRes];
                        radius2 += ((float *)yCoordinates)[x + y*xRes] * ((float *)yCoordinates)[x + y*xRes];
                        radius2 += ((float *)zCoordinates)[x + y*xRes] * ((float *)zCoordinates)[x + y*xRes];
                        if (radius2 < radius2Min) {
                            radius2Min = radius2;
//                            nearestPixelX = x;
//                            nearestPixelY = y;
//                            xCoordinate = ((float *)xCoordinates)[x + y*xRes];
//                            yCoordinate = ((float *)yCoordinates)[x + y*xRes];
                            zCoordinate = ((float *)zCoordinates)[x + y*xRes];

                            data.at<float>(y, x) = zCoordinate;
                        }
                    }
                }
            }
        }
    }
    // The Channels X, Y and Z are given in the predefine Cartesian coordinate system.
    // See Figure "ToF coordinate system".

    cv::rotate(data, data, cv::ROTATE_180);
}

void Argos3DRecorder::getAmplitudes(BTA_Frame *frame, cv::Mat &data){
    BTA_Status btaStatus;
    uint16_t *amplitudes;
    BTA_DataFormat dataFormat;
    BTA_Unit unit;
    uint16_t xRes, yRes;

    btaStatus = BTAgetAmplitudes(frame, (void **)&amplitudes, &dataFormat, &unit, &xRes, &yRes);
    errorHandling(btaStatus);
    if (dataFormat == BTA_DataFormatUInt16) {
        // This dataformat tells us that it's ok to interpret the data as (uint16_t *)
        data = cv::Mat::zeros(yRes, xRes, CV_16UC1);
        if (unit == BTA_UnitUnitLess) {
            // -> access amplitude data simply as amplitudes[i]
            for (int y = 0; y < yRes; y++) {
                for (int x = 0; x < xRes; x++) {
                    data.at<float>(y, x) = amplitudes[x + y*xRes];
                }
            }
        }
    }
    else if (dataFormat == BTA_DataFormatFloat32) {
        data = cv::Mat::zeros(yRes, xRes, CV_32FC1);
        // This dataformat tells us that it's ok to interpret the data as (float *)
        if (unit == BTA_UnitUnitLess) {
            // -> access amplitude data simply as ((float *)amplitudes)[i]
            for (int y = 0; y < yRes; y++) {
                for (int x = 0; x < xRes; x++) {
                    data.at<float>(y, x) = ((float *)amplitudes)[x + y*xRes];

                }
            }
        }
    }
    // The first pixel value in the buffer corresponds to the
    // upper left pixel (sensor point of view).
}