//
// Created by Joël Dumoulin on 12.06.17.
//

#include "RecordingConf.h"

#include <regex>

#include <iostream>


using namespace std;

RecordingConf::RecordingConf(std::string recordingBasePath, std::string sensorId, std::string scenarioId, std::string idxstr)
        : recordingBasePath(recordingBasePath), sensorId(sensorId), scenarioId(scenarioId), idxstr(idxstr){

    this->recordingBasePath /= scenarioId;
    this->prepareDirectories();
}

RecordingConf::~RecordingConf() {}

const std::string &RecordingConf::getSensorId() const {
    return sensorId;
}

const std::string &RecordingConf::getScenarioId() const {
    return scenarioId;
}
//
const std::string &RecordingConf::getIdxstr() const {
    return idxstr;
}
//

boost::filesystem::path RecordingConf::getColorPath() const {
    return recordingBasePath / "color";
}

boost::filesystem::path RecordingConf::getColorHdPath() const {
    return recordingBasePath / "color_hd";
}

boost::filesystem::path RecordingConf::getDepthPath() const {
    return recordingBasePath / "depth";
}

boost::filesystem::path RecordingConf::getIrPath() const {
    return recordingBasePath / "ir";
}

boost::filesystem::path RecordingConf::getRecordingBasePath() const {
    return recordingBasePath;
}

boost::filesystem::path RecordingConf::getKinectParametersPath() const {
    return recordingBasePath / (this->sensorId + ".yml");
}


void RecordingConf::prepareDirectories() {
    bool created = false;
    boost::filesystem::path path;

    int x = 0;
    char recordingId[17];
    char *idxchar = new char[idxstr.length()+1];
    std::strcpy(idxchar,idxstr.c_str());
    sprintf(idxchar, "%03d", stoi(idxstr));


    //
    path= recordingBasePath / idxchar;
    created = createDirectories(path);

//    while (!created) {
//        x++;
//        sprintf(recordingId, "%03d", x);
//        string sub=idxstr+string(recordingId);
//        path = recordingBasePath / sub;
//        created = createDirectories(path);
//    }

    //


    recordingBasePath /= idxchar;
    //
//    recordingBasePath /= string(recordingId);

    recordingBasePath /= sensorId;

    // Check if directories exist. If not, create
    if (createDirectories(getColorPath())) std::cout << "Directory Created: " << getColorPath() << std::endl;

    if (createDirectories(getColorHdPath())) std::cout << "Directory Created: " << getColorHdPath() << std::endl;

    if (createDirectories(getDepthPath())) std::cout << "Directory Created: " << getDepthPath() << std::endl;

    if (createDirectories(getIrPath())) std::cout << "Directory Created: " << getIrPath() << std::endl;

}

bool RecordingConf::createDirectories(boost::filesystem::path path) {
    return boost::filesystem::create_directories(boost::filesystem::path(path));
}
