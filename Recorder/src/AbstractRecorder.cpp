//
// Created by Joël Dumoulin on 11.06.17.
//

#include "AbstractRecorder.h"

#include <boost/array.hpp>




using namespace chrono;


AbstractRecorder::AbstractRecorder() : status(uninitialized), frameWriter() {
    writerWorker = std::thread(&FrameWriter::worker, &frameWriter);

}

AbstractRecorder::~AbstractRecorder() {}

bool AbstractRecorder::isReady() {
    return status == SensorStatus::ready;
}

SensorStatus AbstractRecorder::getStatus() {
    return status;
}

void AbstractRecorder::startRecording(RecordingConf &conf) {
    recordingConf = &conf;

    frameNb = 0;

    startTime = duration_cast<milliseconds>(system_clock::now().time_since_epoch());

    if (status == SensorStatus::ready) {
        status = SensorStatus::recording;
    }
}

long AbstractRecorder::stopRecording() {
    if (status == SensorStatus::recording) {
        status = SensorStatus::ready;
    }

    return frameNb;
}

void AbstractRecorder::showFrames() {
    show = true;
}

void AbstractRecorder::hideFrames() {
    show = false;
}

void AbstractRecorder::stop() {
    status = SensorStatus::stopped;

    recordingWorker.join();
    writerWorker.join();
}