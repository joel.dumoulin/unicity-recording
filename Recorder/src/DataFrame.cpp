//
// Created by Joël Dumoulin on 11.06.17.
//

#include "DataFrame.h"

using namespace cv;
using namespace std;

DataFrame::DataFrame(Mat data, string path, FrameType frameType, cv::RotateFlags rotate) : data(data), path(path),
                                                                                 frameType(frameType), rotate(rotate) {}

DataFrame::~DataFrame() {

}

const cv::Mat &DataFrame::getData() const {
    return data;
}

void DataFrame::setData(const cv::Mat &data) {
    DataFrame::data = data;
}

const std::string &DataFrame::getPath() const {
    return path;
}

void DataFrame::setPath(const std::string &path) {
    DataFrame::path = path;
}

FrameType DataFrame::getFrameType() const {
    return frameType;
}

void DataFrame::setFrameType(FrameType frameType) {
    DataFrame::frameType = frameType;
}

const cv::RotateFlags DataFrame::getRotate() const {
    return rotate;
}

void DataFrame::setRotate(cv::RotateFlags rotate) {
    DataFrame::rotate = rotate;
}
