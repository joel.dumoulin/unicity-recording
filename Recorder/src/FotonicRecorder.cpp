//
// Created by Joël Dumoulin on 11.06.17.
//

#include "FotonicRecorder.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <thread>
#include <chrono>

#include <fstream>
#include <iostream>
#include <iomanip>
#include <opencv2/core.hpp>


const int MAX_STR_LENGTH = 512;

using namespace GenTL;

using namespace std;
using namespace chrono;

FotonicRecorder::FotonicRecorder() : AbstractRecorder() {
    iface = NULL;
    device = NULL;
    dataStream = NULL;
}

FotonicRecorder::~FotonicRecorder() {
    IFClose(iface);
    DevClose(device);
}

bool FotonicRecorder::initSensor() {
    TL_HANDLE system(NULL);

    GC_ERROR err = GCInitLib();

    if (GC_ERR_SUCCESS == err)
    {
        err = TLOpen(&system);
    }

    if (GC_ERR_SUCCESS == err)
    {
        err = TLUpdateInterfaceList(system, NULL, GENTL_INFINITE);
    }

    if (GC_ERR_SUCCESS == err)
    {
        menuInterface(system);
    }

    status = SensorStatus::ready;

    std::cout << "Sensor initialized!" << std::endl;

    recordingWorker = std::thread(&FotonicRecorder::run, this);

    return true;
}

void FotonicRecorder::run() {

    std::cout << "Fotonic sensor grabbing frames!!!" << std::endl;

    std::chrono::high_resolution_clock::time_point p;
    std::string now;
    char duration[17];
    char strFrameNb[17];
    milliseconds nowTime, durationTime;

    cv::Mat depth, amplitude;

    boost::filesystem::path path;

    // Prepare fotonic
    size_t size = sizeof(size_t);
    size_t payloadSize = 0;
    INFO_DATATYPE type;
    GC_ERROR err = DSGetInfo(dataStream, STREAM_INFO_PAYLOAD_SIZE, &type, &payloadSize, &size);

    const int nBuffers = 4;
    BUFFER_HANDLE buffers[nBuffers];

    for (int i = 0; i < nBuffers; i++)
    {
        if (GC_ERR_SUCCESS == err)
        {
            err = DSAllocAndAnnounceBuffer(dataStream, payloadSize, NULL, &(buffers[i]));
        }

        if (GC_ERR_SUCCESS == err)
        {
            err = DSQueueBuffer(dataStream, buffers[i]);
        }
    }

    EVENT_HANDLE newBufferEvent;

    // register to obtain a notification about buffer events.
    if (GC_ERR_SUCCESS == err)
    {
        err = GCRegisterEvent(dataStream, EVENT_NEW_BUFFER, &newBufferEvent);
    }

    bool acquisitionStarted = false;

    if (GC_ERR_SUCCESS == err) {
        while (status == SensorStatus::ready || status == SensorStatus::recording) {
            if (status == SensorStatus::recording) {
                if(!acquisitionStarted){
                    err = DSStartAcquisition(dataStream, ACQ_START_FLAGS_DEFAULT, GENTL_INFINITE);
                    acquisitionStarted = true;
                }

                // Compute timestamp
                nowTime = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
                now = std::to_string(nowTime.count());

                durationTime = nowTime - startTime;

                sprintf(duration, "%05lu", size_t(durationTime.count()));

                sprintf(strFrameNb, "%05ld", frameNb);

                frameNb++;

                if (ENABLE_DEPTH) {
                    this->getDepth(&newBufferEvent, depth);

                    path = recordingConf->getDepthPath();
                    path /= now + "_" + string(duration) + "_" + string(strFrameNb);
                    frameWriter.add(depth, path.string(), FrameType::depth, cv::ROTATE_90_COUNTERCLOCKWISE);
                }
            }else{
                if(acquisitionStarted){
                    DSStopAcquisition(dataStream, ACQ_STOP_FLAGS_DEFAULT);
                    acquisitionStarted = false;
                }
            }
        }
    }

    frameWriter.stop();

    // Disconnecting
    //----------------------------------------------------------------------------------------------
    DSClose(dataStream);
    DevClose(device);
    IFClose(iface);

    std::cout << "Stopped" << std::endl;
}


// Choose interface
int FotonicRecorder::menuInterface(TL_HANDLE system)
{
    uint32_t numInterfaces(0);
    TLGetNumInterfaces(system, &numInterfaces);
    std::cout << "Found " << numInterfaces << " interfaces: " << std::endl;

    int nr = -1;

    char id[MAX_STR_LENGTH];
    std::cout << "[0]: " << "exit" << "\n";

    for (size_t i = 0; i < numInterfaces; i++)
    {
        size_t size = sizeof(id);
        GC_ERROR err = TLGetInterfaceID(system, i, id, &size);

        if (err == GC_ERR_SUCCESS)
        {
            std::cout << "[" << i + 1 << "]: " << id << "\n";
        }
        else
        {
            std::cout << "[" << i + 1 << "]: " << "failed to load interface id" << "\n";
        }
    }

    do{
        std::cout << "option: ";
        if (!std::cin.good())
        {
            std::cin.clear();
        }
        std::cin >> nr;
    }while(nr < 0 || nr > numInterfaces);

    size_t ifaceNr = nr - 1;

    if (ifaceNr >= 0 && ifaceNr < numInterfaces)
    {
        size_t size = MAX_STR_LENGTH;
        GC_ERROR err = TLGetInterfaceID(system, ifaceNr, id, &size);

        if (GC_ERR_SUCCESS == err)
        {
            err = TLOpenInterface(system, id, &iface);
        }

        if (GC_ERR_SUCCESS == err)
        {
            menuDevice(iface);
        }
    }

    return 0;
}


// Choose device
int FotonicRecorder::menuDevice(IF_HANDLE iface)
{
    size_t size;
    uint32_t numDevices(0);

    GC_ERROR err = IFUpdateDeviceList(iface, NULL, GENTL_INFINITE);

    if (GC_ERR_SUCCESS == err)
    {
        err = IFGetNumDevices(iface, &numDevices);
    }

    if (GC_ERR_SUCCESS != err)
    {
        iface = NULL;
    }
    else
    {
        std::cout << "Found " << numDevices << " devices: " << std::endl;
        size_t nr = -1;


        char id[MAX_STR_LENGTH];

        // print table
        std::cout << "[0]: " << "exit" << "\n";

        for (size_t i = 0; i < numDevices; i++)
        {
            size_t size = sizeof(id);
            GC_ERROR err = IFGetDeviceID(iface, i, id, &size);

            if (err == GC_ERR_SUCCESS)
            {
                std::cout << "[" << i + 1 << "]: " << id << "\n";
            }
            else
            {
                std::cout << "[" << i + 1 << "]: " << "failed to load device id" << "\n";
            }
        }


        do{
            std::cout << "option: ";
            if (!std::cin.good())
            {
                std::cin.clear();
            }
            std::cin >> nr;
        }while(nr < 0 || nr > numDevices);

        size_t devNr = nr - 1;

        if (devNr >= 0 && devNr < numDevices)
        {
            uint32_t numUrls = 0;
            size_t typeSize = sizeof(URL_SCHEME_ID);
            INFO_DATATYPE type(0);
            size = sizeof(id);

            GC_ERROR err = IFGetDeviceID(iface, devNr, id, &size);

            if (GC_ERR_SUCCESS == err)
            {
                err = IFOpenDevice(iface, id, DEVICE_ACCESS_EXCLUSIVE, &device);
            }

            if (GC_ERR_SUCCESS == err && device != NULL)
            {
                std::cout << "device opened" << std::endl;

                // Get depth stream (*_XYZ)
                char id[MAX_STR_LENGTH];
                size_t size = sizeof(id);
                GC_ERROR err = DevGetDataStreamID(device, 0, id, &size);

                size = sizeof(id);
                err = DevGetDataStreamID(device, 0, id, &size);

                if (GC_ERR_SUCCESS == err)
                {
                    err = DevOpenDataStream(device, id, &dataStream);
                }

                if (GC_ERR_SUCCESS != err){
                    std::cout << "Failed to open Fotonic depth stream" << std::endl;
                }
            }
            else
            {
                std::cout << "failed to connect to device (gc error " << err << ")" << std::endl;
            }
        }
    }

    return 0;
}

void FotonicRecorder::getDepth(EVENT_HANDLE *bufferEvent, cv::Mat &data){
#define Coord3D_C16			0x011000B8	//	PFNC		3D coordinate C 16-bit - corresponds to Z
    size_t size = sizeof(size_t);
    size_t outSize;
    size_t eventDataSize;
    outSize = sizeof(eventDataSize);
    GC_ERROR err;
    INFO_DATATYPE type;

    // obtain maximum event size
    err = EventGetInfo(*bufferEvent, EVENT_SIZE_MAX, &type, &eventDataSize, &outSize);

    std::vector<uint8_t> eventData(eventDataSize);
    BUFFER_HANDLE buffer = 0;
    uint32_t numParts = 0;

    // wait for buffer event (see registration above)
    err = EventGetData(*bufferEvent, (void*) eventData.data(), &eventDataSize, 1000 /* ms */);

    if (GC_ERR_SUCCESS == err)
    {
        //std::cout << "received event" << std::endl;

        size = sizeof(buffer);

        // obtain buffer handle and size, type
        err = EventGetDataInfo(*bufferEvent, (void*) eventData.data(), eventDataSize
                , EVENT_DATA_ID, &type, &buffer, &size);
    }
    else if (GC_ERR_TIMEOUT == err)
    {
        std::cout << "timeout\nUse AcquisitionControl to start the remote device" << std::endl;

    }

    if (GC_ERR_SUCCESS == err)
    {
        //std::cout << "received event info" << std::endl;

        // identify the number of parts (i.e. image channels) the buffer contains
        err = DSGetNumBufferParts(dataStream, buffer, &numParts);
    }

    if (GC_ERR_SUCCESS == err)
    {
        //std::cout << "num parts: " << numParts << std::endl;

        // iterate over all parts
        for (size_t i = 0; i < numParts; i++)
        {
            struct
            {
                void* address;
                size_t bytes;
                size_t type;			// of type enum PARTDATATYPE_IDS, see http://www.emva.org/wp-content/uploads/GenICam_GenTL_1_5.pdf
                uint64_t format;		// see genicam pixel format definitions at http://www.emva.org/wp-content/uploads/GenICam_PixelFormatValues.pdf
                uint64_t formatNs;		// of type enum PIXELFORMAT_NAMESPACE_IDS, should default to PIXELFORMAT_NAMESPACE_PFNC_32BIT (4)
                size_t width;
                size_t height;
            } part;

            size = sizeof(part.format);

            if (GC_ERR_SUCCESS == err)
            {
                err = DSGetBufferPartInfo(dataStream, buffer, i, BUFFER_PART_INFO_DATA_FORMAT, &type, &part.format, &size);
            }

            if(part.format == Coord3D_C16){
                size = sizeof(part.address);

                if (GC_ERR_SUCCESS == err)
                {
                    err = DSGetBufferPartInfo(dataStream, buffer, i, BUFFER_PART_INFO_BASE, &type, &part.address, &size);
                }

                size = sizeof(part.width);

                if (GC_ERR_SUCCESS == err)
                {
                    err = DSGetBufferPartInfo(dataStream, buffer, i, BUFFER_PART_INFO_WIDTH, &type, &part.width, &size);
                }

                size = sizeof(part.height);

                if (GC_ERR_SUCCESS == err)
                {
                    err = DSGetBufferPartInfo(dataStream, buffer, i, BUFFER_PART_INFO_HEIGHT, &type, &part.height, &size);
                }

                uint16_t* im = (uint16_t*) part.address;

                data = cv::Mat::zeros(part.height, part.width, CV_32FC1);

                for(size_t j = 0; j < part.height; j++)
                {
                    for (size_t i = 0; i < part.width; i++)
                    {
                        data.at<float>(j, i) = (int) im[j * part.width + i];
                    }
                }
            }
        }
    }

    // place the buffer back to the queue
    if (buffer)
    {
        err = DSQueueBuffer(dataStream, buffer);
    }

    //size = sizeof(isGrabbing);
    //err = DSGetInfo(dataStream, STREAM_INFO_IS_GRABBING, &type, &isGrabbing, &size);
}