//
// Created by Joël Dumoulin on 12.06.17.
//

#ifndef UNICITYRECORDING_RECORDINGCONF_H
#define UNICITYRECORDING_RECORDINGCONF_H

#include <string>

#include <boost/filesystem.hpp>

class RecordingConf {
public:
    RecordingConf(std::string recordingBasePath, std::string sensorId, std::string scenarioId, std::string idxstr);

    ~RecordingConf();


    const std::string &getSensorId() const;

    const std::string &getScenarioId() const;
    //
    const std::string &getIdxstr() const;
    //

    boost::filesystem::path getColorPath() const;

    boost::filesystem::path getColorHdPath() const;

    boost::filesystem::path getDepthPath() const;

    boost::filesystem::path getIrPath() const;

    boost::filesystem::path getRecordingBasePath() const;

    boost::filesystem::path getKinectParametersPath() const;

    void prepareDirectories();


private:
    std::string sensorId;
    std::string scenarioId;
    std::string idxstr;

    boost::filesystem::path recordingBasePath;

    bool createDirectories(boost::filesystem::path path);
};

#endif //UNICITYRECORDING_RECORDINGCONF_H
