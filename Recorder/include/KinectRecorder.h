//
// Created by Joël Dumoulin on 11.06.17.
//

#ifndef UNICITY_RECORDING_KINECTRECORDER_H
#define UNICITY_RECORDING_KINECTRECORDER_H

#include "AbstractRecorder.h"

#include "../../include/k2g.h"
//#include <pcl/visualization/cloud_viewer.h>

#include <boost/filesystem.hpp>

#include <string>

class KinectRecorder : public AbstractRecorder {
public:
    KinectRecorder();

    ~KinectRecorder();

    virtual bool initSensor();

    void saveParameters(std::string kinectParametersPath);

private:
    virtual void run();

    const bool ENABLE_RGB = true;
    const bool ENABLE_RGB_HD = true;
    const bool ENABLE_DEPTH = true;
    const bool ENABLE_IR = true;

    K2G *grabber;
    //boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
    boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> cloud;
};

#endif //UNICITY_RECORDING_KINECTRECORDER_H
