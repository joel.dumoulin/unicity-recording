//
// Created by Joël Dumoulin on 11.06.17.
//

#ifndef UNICITY_RECORDING_ARGOS3DRECORDER_H
#define UNICITY_RECORDING_ARGOS3DRECORDER_H

#include "AbstractRecorder.h"
#include <opencv2/core.hpp>

#include <bta.h>

class Argos3DRecorder : public AbstractRecorder {
public:
    Argos3DRecorder();

    ~Argos3DRecorder();

    virtual bool initSensor();

private:
    virtual void run();

    void getDepth(BTA_Frame *frame, cv::Mat &data);
    void getAmplitudes(BTA_Frame *frame, cv::Mat &data);

    const bool ENABLE_DEPTH = true;
    const bool ENABLE_AMPLITUDE = false;

    BTA_Handle* btaHandle;
};


static void errorHandling(BTA_Status status) {
    if (status != BTA_StatusOk) {
        char statusString[100];
        BTAstatusToString(status, statusString, (uint16_t)sizeof(statusString));
        printf("error: %s (%d)\n", statusString, status);
        printf("Hit <Return> to end the example\n");
        fgetc(stdin);
        exit(0);
    }
}

#endif //UNICITY_RECORDING_ARGOS3DRECORDER_H
