//
// Created by Joël Dumoulin on 11.06.17.
//

#ifndef UNICITY_RECORDING_FOTONICRECORDER_H
#define UNICITY_RECORDING_FOTONICRECORDER_H

#include "AbstractRecorder.h"

#include <GenTL_v1_5.h>


#include <GenICam.h>
#include <GenApi/PortImpl.h>

using namespace GenTL;

class FotonicRecorder : public AbstractRecorder {
public:
    FotonicRecorder();

    ~FotonicRecorder();

    virtual bool initSensor();

private:
    virtual void run();

    int menuInterface(TL_HANDLE system);
    int menuDevice(IF_HANDLE iface);

    void getDepth(EVENT_HANDLE *bufferEvent, cv::Mat &data);

    IF_HANDLE iface;
    DEV_HANDLE device;
    DS_HANDLE dataStream;

    const bool ENABLE_DEPTH = true;

};

// ----------------------------------------------------------------------------
// Transport layer between GenTL and GenAPI
// ----------------------------------------------------------------------------
class TransportLayer : public GenApi::CPortImpl
{
public:
    // ----------------------------------------------------------------------------
    //
    // ----------------------------------------------------------------------------
    TransportLayer(GenTL::PORT_HANDLE portHandle)
            :mPortHandle(portHandle)
    {
    }

    // ----------------------------------------------------------------------------
    //
    // ----------------------------------------------------------------------------
    TransportLayer(TransportLayer& tl)
            :mPortHandle(tl.mPortHandle)
    {
    }

    // ----------------------------------------------------------------------------
    //
    // ----------------------------------------------------------------------------
    virtual GenApi::EAccessMode GetAccessMode() const
    {
        // If the driver is open, return RW (= read/write),
        // otherwise NA (= not available)
        return GenApi::RW;
    }

    // ----------------------------------------------------------------------------
    //
    // ----------------------------------------------------------------------------
    virtual void Read(void* buffer, int64_t address, int64_t length)
    {
        size_t size = (size_t) length;
        GenTL::GCReadPort(mPortHandle, address, buffer, &size);
    }

    // ----------------------------------------------------------------------------
    //
    // ----------------------------------------------------------------------------
    virtual void Write(const void* buffer, int64_t address, int64_t length)
    {
        size_t size = (size_t) length;
        GenTL::GCWritePort(mPortHandle, address, buffer, &size);
    }
private:
    GenTL::PORT_HANDLE mPortHandle;
};

#endif //UNICITY_RECORDING_FOTONICRECORDER_H
