//
// Created by Joël Dumoulin on 11.06.17.
//

#ifndef UNICITY_RECORDING_ENUMS_H
#define UNICITY_RECORDING_ENUMS_H

enum SensorStatus {uninitialized, ready, recording, stopped};

enum FrameType {color, color_hd, depth, ir};

#endif //UNICITY_RECORDING_ENUMS_H
