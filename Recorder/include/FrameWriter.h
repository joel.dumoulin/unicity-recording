//
// Created by Joël Dumoulin on 11.06.17.
//

#ifndef UNICITY_RECORDING_FRAMEWRITER_H
#define UNICITY_RECORDING_FRAMEWRITER_H

#include "DataFrame.h"
#include "Enums.h"
#include "../../include/wqueue.h"
#include <opencv2/opencv.hpp>


class FrameWriter {
public:

    FrameWriter();

    ~FrameWriter();

    void worker();

    void add(cv::Mat data, std::string path, FrameType frameType, cv::RotateFlags rotate);

    void stop();

    int getSize();

private:
    bool run;

    wqueue<DataFrame *> queue;

    const int JPEG_QUALITY = 100;

    void matwrite(const string& filename, const cv::Mat& mat);
    cv::Mat matread(const string& filename);
};

#endif //UNICITY_RECORDING_FRAMEWRITER_H
