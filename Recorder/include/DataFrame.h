//
// Created by Joël Dumoulin on 11.06.17.
//

#ifndef UNICITY_RECORDING_DATAFRAME_H
#define UNICITY_RECORDING_DATAFRAME_H

#include <opencv2/opencv.hpp>
#include <string>

#include "Enums.h"

class DataFrame {
public:

    DataFrame();

    DataFrame(cv::Mat data, std::string path, FrameType frameType, cv::RotateFlags);

    ~DataFrame();

    const cv::Mat &getData() const;

    void setData(const cv::Mat &data);

    const std::string &getPath() const;

    void setPath(const std::string &path);

    FrameType getFrameType() const;

    void setFrameType(FrameType frameType);

    const cv::RotateFlags getRotate() const;

    void setRotate(cv::RotateFlags rotate);

private:
    cv::Mat data;
    std::string path;
    FrameType frameType;
    cv::RotateFlags rotate;
};

#endif //UNICITY_RECORDING_DATAFRAME_H
