//
// Created by Joël Dumoulin on 11.06.17.
//

#ifndef UNICITY_RECORDING_ABSTRACTRECORDER_H
#define UNICITY_RECORDING_ABSTRACTRECORDER_H

#include "Enums.h"
#include "FrameWriter.h"
#include "RecordingConf.h"

#include <thread>
#include <chrono>

class AbstractRecorder {
public:
    AbstractRecorder();

    virtual ~AbstractRecorder();

    virtual bool initSensor() = 0;

    bool isReady();

    SensorStatus getStatus();

    void startRecording(RecordingConf &recordingConf);

    long stopRecording();

    void showFrames();

    void hideFrames();

    void stop();

protected:
    virtual void run() = 0;

    atomic<SensorStatus> status;

    FrameWriter frameWriter;

    bool show;

    thread recordingWorker;
    thread writerWorker;

    RecordingConf *recordingConf;

    long frameNb;
    chrono::milliseconds startTime;
};

#endif //UNICITY_RECORDING_ABSTRACTRECORDER_H
