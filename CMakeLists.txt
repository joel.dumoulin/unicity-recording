cmake_minimum_required(VERSION 3.7)
project(UnicityRecording CXX)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/modules")
set(LIB_PATH "${CMAKE_SOURCE_DIR}/lib")

# ----------------------------------------------------------------------------------------------------------------------
# Platforms (can be set when calling CMAKE. For instance: -DMACOS=ON)
# ----------------------------------------------------------------------------------------------------------------------
#set(MACOS ON)
#set(LINUX OFF)
#set(WINDOWS OFF)

if(LINUX)
    add_definitions(-DLINUX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")
elseif(MACOS)
    add_definitions(-DMACOS)
endif()


subdirs(Recorder)
