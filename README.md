# Installation

## Install on Linux

### Install OpenCV 3.2.0

* http://docs.opencv.org/3.2.0/d7/d9f/tutorial_linux_install.html

If not finding libopencv_core.so.2.4:

Make symbolic links in /usr/local/lib and then run program with:

```
sudo ln -s libopencv_core.so.3.2 libopencv_core.so.2.4
sudo ln -s libopencv_imgproc.so.3.2 libopencv_imgproc.so.2.4
sudo -E LD_LIBRARY_PATH=/usr/local/lib ./TestRecorder
```

### Install Boost 1.64

Download and extract:

```
wget -O boost_1_64_0.tar.gz http://sourceforge.net/projects/boost/files/boost/1.64.0/boost_1_64_0.tar.gz/download
tar xzvf boost_1_64_0.tar.gz
cd boost_1_64_0/
```

Install required libraries:

```
sudo apt-get update
sudo apt-get install build-essential g++ python-dev autotools-dev libicu-dev libbz2-dev
```

Prepare, build and install:

```
./bootstrap.sh --prefix=/usr/local
./b2
sudo ./b2 install
```

### Install PCL 1.8.0

```
sudo add-apt-repository ppa:v-launchpad-jochen-sprickerhof-de/pcl
sudo apt-get update
sudo apt-get install libpcl-dev
```

If problem : "runtime library [libmpi.so] in /usr/lib may be hidden by files in: /usr/lib/x86_64-linux-gnu/openmpi/lib":

* Just remove "/usr/lib/libmpi.so" from VTK_LIBRARIES in /usr/lib/x86_64-linux-gnu/cmake/pcl/PCLConfig.cmake. 

### Install libfreenect2

* https://github.com/OpenKinect/libfreenect2/blob/master/README.md#linux

* Copy/move freenect2/include/libfreenect2 in /usr/include/libfreenect2
* Copy/move freenect2/lib in /usr/lib
* Install libusb-0.1-4 (needed for ARGOS)

```
sudo apt-get install libusb-0.1-4
```

### Install YAML-CPP

```
sudo apt-get install libyaml-cpp-dev
```

### Install Qt

```
sudo apt-get install qtdeclarative5-dev
```

## Install on MacOS

### Install OpenCV 3.2.0

```
 brew install opencv3 # will install last 3.x version, but it's ok
```

### Install Boost 1.64

```
brew install boost
```

### Install PCL 1.8.0

```
brew install pcl
```

### Install libfreenect2

* https://github.com/OpenKinect/libfreenect2/blob/master/README.md#mac-osx

## Additional notes

### Problem with libmpi.so on Ubuntu

* Make a symbolic link for the library in /usr/lib

```
sudo ln -s /usr/lib/x86_64-linux-gnu/libmpi.so libmpi.so
```

### Install YAML-CPP

```
brew install yaml-cpp
```

# Time synchronization

## MacOs

Activate automatic time and set ntp server in Preferences -> Date and time -> Set to: *Ubuntu (ntp.ubuntu.com)*

## Linux

Use *timedatectl* to ensure that *NTP synchronized* is set to yes. If not:

```
timedatectl set-ntp true
```

NTP server url can then be set:

```
sudo nano /etc/systemd/timesyncd.conf
NTP=ntp.ubuntu.com
```

More information: https://help.ubuntu.com/lts/serverguide/NTP.html

# Configure CLion

In *Preferences/Build, Execution, Deployment/CMake* :

CMake options: -DLINUX=ON -DCMAKE_PREFIX_PATH=${Qt cmake path}

For instance on mac: ${Qt cmake path} = /Users/joel/dev/libs/**Qt/5.9/clang_64/lib/cmake**

